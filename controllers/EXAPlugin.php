<?php

namespace controllers;

class EXAPlugin extends \controllers\base\EXABase{

    public function execute() {
        //\daos\EXADaoExample::createTable();

        $exaAdminController = new \controllers\admin\EXAAdmin();
        $exaShopController = new \controllers\shop\EXAShop();

        $exaAdminController->execute();
        $exaShopController->execute();
    }
}