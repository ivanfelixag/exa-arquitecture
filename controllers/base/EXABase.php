<?php

namespace controllers\base;

abstract class EXABase {

    public abstract function execute();
}