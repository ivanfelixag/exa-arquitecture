<?php

namespace controllers\admin;

use utils\EXAEyes;

class EXAAdmin extends \controllers\base\EXABase {

    public function execute() {
        EXAEyes::getInstance()->blink('admin', 'view', array('admin'), array('admin'));
    }
}

