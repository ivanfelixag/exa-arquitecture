<?php

namespace daos\base;

use utils\EXADBConnector;

class EXABase {

    protected static function isInstalled() {
        $exaTablesPrefix = EXADBConnector::getInstance()->getTablesPrefix();
        $query = "SHOW TABLES LIKE '{$exaTablesPrefix}%'";
        $numTables = EXADBConnector::getInstance()->query($query);
        return ($numTables == 2);
    }

}