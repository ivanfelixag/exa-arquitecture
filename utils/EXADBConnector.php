<?php

namespace utils;

class EXADBConnector {

    private $exaTablesPrefix;
    private $exaTables;
    protected static $instance;

    public function __construct() {
        global $wpdb;

        $this->exaTablesPrefix = $wpdb->prefix . 'exa_wl';
        $this->exaTables = array(
            "whislistsTable" => $this->exaTablesPrefix . '_whislists',
            "whislistsItemsTable" => $this->exaTablesPrefix . '_whislists_items'
        );
    }

    public function getTablesPrefix() {
        return $this->exaTablesPrefix;
    }

    public function getTables() {
        return $this->exaTables;
    }

    public function query($query) {
        global $wpdb;

        return $wpdb->query($query);
    }

    public static function getInstance() {
        if(is_null(self::$instance)) {
            self::$instance = new EXADBConnector();
        }
        return self::$instance;
    }

}