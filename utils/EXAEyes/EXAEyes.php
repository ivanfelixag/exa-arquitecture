<?php

namespace utils;

class EXAEyes {
    private static $instance;

    private function enqueueScript($folder, $scriptName, $inFooter) {
        wp_enqueue_script(
            $scriptHandler = 'script-' . $folder . '-' . $scriptName,
            $scriptSRC = EXA_PLUGIN_URL . 'resources/assets/' . $folder . '/js/' . $scriptName . '.js',
            $dependencies = array(),
            $version = EXA_VERSION,
            $inFooter);
    }

    private function enqueueStyle($folder, $styleName) {
        wp_enqueue_style(
            $styleHandler = 'style-' . $folder . '-' . $styleName,
            $styleSRC = EXA_PLUGIN_URL . 'resources/assets/' . $folder . '/css/' . $styleName . '.css',
            $dependencies = array(),
            $version = EXA_VERSION);
    }

    public function blink($folder, $view, $scripts = array(), $styles = array()) {
        foreach ($styles as $styleName) {
            $this->enqueueStyle($folder, $styleName);
        }
        foreach($scripts as $scriptName) {
            $this->enqueueScript($folder, $scriptName, $inFooter = true);
        }

        ob_start();
        include(EXA_BASE_PATH . 'views/' . $folder . "/" . $view . '.php');
        $fileContent = ob_get_contents();
        ob_end_clean ();
        echo $fileContent;
    }

    public static function getInstance() {
        if(is_null(self::$instance)) {
            self::$instance = new EXAEyes();
        }
        return self::$instance;
    }
}