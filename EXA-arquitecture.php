<?php
/**
 * @package EXA_arquitecture
 */
/*
Plugin Name: EXA arquitecture
Description: Plugin Skull.
Version: 1.0.0
Author: Nemelomello@ExaCodes | Murugh@ExaCodes
Text Domain: exa-arquitecture
*/

// ------ GLOBAL VARIABLES AREA ------
define('EXA_VERSION', '1.0.0');
define('EXA_BASE_PATH', plugin_dir_path( __FILE__ ));
define('EXA_PLUGIN_URL', plugin_dir_url( __FILE__ ));

// ------         LOADER        ------
require_once 'controllers/base/EXABase.php';
require_once 'controllers/admin/EXAAdmin.php';
require_once 'controllers/shop/EXAShop.php';
require_once 'controllers/EXAPlugin.php';
require_once 'daos/base/EXABase.php';
require_once 'utils/EXAEyes/EXAEyes.php';
require_once 'utils/EXADBConnector.php';

// ------       EXECUTE        ------
$exaPlugin = new \controllers\EXAPlugin();
$exaPlugin->execute();